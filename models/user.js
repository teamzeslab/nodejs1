var mongoose = require('mongoose')

/////////********  mongoose model
var Users = mongoose.model('Users',{
  text:{
    type: String,
    required: true,
    minlength: 2,
    trim: true
  },
  mobile:{
    type: String,
    require:true
  },
  email:{
    type: String,
    require: true
  },
  password:{
    type: String,
    require: true
  },
  completed:{
    type: Boolean,
    default: false
  },
  completedAt:{
    type: Number,
    default: null
  }
});

module.exports={Users}
